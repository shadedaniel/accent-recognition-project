#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 02:38:00 2018

@author: folashade
"""

#import glob
#import os
import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram


#def plot_waves(raw_sound):
#    plt.figure(figsize=(10,10), dpi = 200)
#    plt.subplot(10,1,1)
#    librosa.display.waveplot(np.array(raw_sound),sr=22050)
#    plt.title("Sound wave plot")
#    plt.suptitle("Figure 1: Waveplot",x=0.5, y=0.915,fontsize=18)
#    plt.show()
    
#def plot_specgram(raw_sound):
#    plt.figure(figsize=(10,10), dpi = 200)
#    plt.subplot(10,1,1)
#    specgram(np.array(raw_sound), Fs=22050)
#    plt.title("Sound spectogram plot")
#    plt.suptitle("Figure 2: Spectrogram",x=0.5, y=0.915,fontsize=18)
#    plt.show()
    
def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name, sr=None)
    #stft = np.abs(librosa.stft(X))
    mfcc = librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=13)
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=13).T,axis=0)
    #chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
    #mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T,axis=0)
    #contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
    #tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T,axis=0)
    return mfccs, mfcc    #,chroma,mel,contrast,tonnetz
    
X, sr = librosa.load("005.wav", sr=None, mono=False)
#plot_waves(X)
#plot_specgram(X)


mfccs, mfcc = extract_feature("005.wav")

