#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 03:48:18 2018

@author: folashade
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.io.wavfile as sw

from scipy.fftpack import dct

# constants (default values)

pre_emphasis = 0.97
frame_size = 0.02
frame_stride = 0.01
n_filt = 40
NFFT = 512
num_ceps = 12
cep_lifter = 22


# SETUP

sample_rate, signal = sw.read("wav_file/005.wav")    # sample_rate is 44100
#signal = signal[0 : int(4 * sample_rate)]   # this accesses the first 4 seconds of the recording

#plt.plot(signal)
#plt.show()

# PRE-EMPHASIS

emphasized_signal = np.append(signal[0], signal[1:] - pre_emphasis * signal[: -1] )
#plt.plot(emphasized_signal)
#plt.show()

# FRAMING

frame_length, frame_step = frame_size * sample_rate, frame_stride * sample_rate     # this converts from seconds to samples
signal_length = len(emphasized_signal)
frame_length = int(round(frame_length))
frame_step = int(round(frame_step))
num_frames = int(np.ceil(float(np.abs(signal_length - frame_length)) / frame_step))  # there might be samples left over

pad_signal_length = num_frames * frame_step + frame_length    # get the total number needed
z = np.zeros((pad_signal_length - signal_length))
pad_signal = np.append(emphasized_signal, z)
# Pad Signal to make sure that all frames have equal number of samples 
#without truncating any samples from the original signal... this will return
#an array padded with zeroes to fill up the missing values of the signals.

indices = np.tile(np.arange(0, frame_length), (num_frames, 1)) + np.tile(np.arange(0, num_frames * frame_step, frame_step), (frame_length, 1)).T
frames = pad_signal[indices.astype(np.int32, copy=False)]

# WINDOWING

frames *= np.hamming(frame_length)

# frames *= 0.54 - 0.46 * numpy.cos((2 * numpy.pi * n) / (frame_length - 1))   ...   explicit formular for hamming window

# FOURIER TRANSFORM ON FRAMES

mag_frames = np.absolute(np.fft.rfft(frames, NFFT))     # magnitude of the fast fourier transform
pow_frames = (1.0 / NFFT) * ((mag_frames) ** 2)     # power spectrum

# FILTER BANKS

low_freq_mel = 0
high_freq_mel = (2595 * np.log10(1 + (sample_rate/2) / 700))    # mel frequency formular: converts Hz to Mel
mel_points = np.linspace(low_freq_mel, high_freq_mel, n_filt + 2)       # equally spaced in the mel scale
hz_points = (700 * (10 ** (mel_points / 2595) -1))      # convert Mel to Hz
bin = np.floor((NFFT + 1) * hz_points / sample_rate)

fbank = np.zeros((n_filt, int(np.floor(NFFT / 2 + 1))))

for m in range(1, n_filt + 1):
    f_m_minus = int(bin[m-1])
    f_m = int(bin[m])
    f_m_plus = int(bin[m+1])
    
    for k in range(f_m_minus, f_m):
        fbank[m-1, k] = (k - bin[m-1]) / (bin[m] - bin[m - 1])
        
    for k in range(f_m, f_m_plus):
        fbank[m - 1, k] = (bin[m + 1] - k) / (bin[m + 1] - bin[m])
        
filter_banks = np.dot(pow_frames, fbank.T)
filter_banks = np.where(filter_banks == 0, np.finfo(float).eps, filter_banks)  # Numerical Stability
filter_banks = 20 * np.log10(filter_banks)  # dB


#plt.plot(filter_banks)
#plt.show()

# MFCC

mfcc = dct(filter_banks, type=2, axis=1, norm='ortho')[:, 1 : (num_ceps + 1)]       # keep coefficients 2-13
(nframes, ncoeff) = mfcc.shape
n = np.arange(ncoeff)
lift = 1 + (cep_lifter / 2) * np.sin(np.pi * n / cep_lifter)
mfcc *= lift


# MEAN NORMALISATION

filter_banks -= (np.mean(filter_banks, axis=0) + 1e-8)

mfcc = np.mean(mfcc.T, axis=1)